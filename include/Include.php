<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/migration/library/ConstMigration.php');
include($strRootPath . '/src/migration/model/MigEntity.php');
include($strRootPath . '/src/migration/model/MigEntityCollection.php');
include($strRootPath . '/src/migration/model/MigEntityFactory.php');
include($strRootPath . '/src/migration/model/repository/MigEntityRepository.php');
include($strRootPath . '/src/migration/model/repository/MigEntityCollectionRepository.php');

include($strRootPath . '/src/migration/sql/library/ConstSqlMigration.php');
include($strRootPath . '/src/migration/sql/model/SqlMigEntity.php');
include($strRootPath . '/src/migration/sql/model/SqlMigEntityFactory.php');
include($strRootPath . '/src/migration/sql/model/repository/SqlMigEntityRepository.php');
include($strRootPath . '/src/migration/sql/model/repository/SqlMigEntityCollectionRepository.php');

include($strRootPath . '/src/build/library/ConstBuilder.php');
include($strRootPath . '/src/build/exception/MigEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/build/api/BuilderInterface.php');
include($strRootPath . '/src/build/model/DefaultBuilder.php');