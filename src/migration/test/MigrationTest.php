<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load external library test
require_once($strRootAppPath . '/vendor/liberty_code/validation/test/validator/boot/ValidatorBootstrap.php');

// Use
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\di\dependency\preference\model\Preference;
use liberty_code\di\dependency\model\DefaultDependencyCollection;
use liberty_code\di\provider\model\DefaultProvider;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\datetime\factory\library\ConstDateTimeFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\model\datetime\factory\model\DefaultDateTimeFactory;
use liberty_code\sql\validation\rule\sql_exist\model\ExistSqlRule;
use liberty_code\migration\migration\factory\model\DefaultMigrationFactory;
use liberty_code\migration\build\directory\model\DirBuilder;
use liberty_code\migration_model\migration\sql\model\SqlMigEntityFactory;
use liberty_code\migration_model\build\model\DefaultBuilder;



// Init DI
$objRegister = new MemoryRegister();
$objDepCollection = new DefaultDependencyCollection($objRegister);
$objProvider = new DefaultProvider($objDepCollection);



// Init validator
$objPref = new Preference(array(
    'source' => ValidatorInterface::class,
    'set' =>  ['type' => 'instance', 'value' => $objValidator],
    'option' => [
        'shared' => true
    ]
));
$objProvider->getObjDependencyCollection()->setDependency($objPref);



// Add rule
$objExistSqlRule = new ExistSqlRule();

$tabRule = array(
    $objExistSqlRule
);
$objRuleCollection->setTabRule($tabRule);



// Init datetime factory
$tabConfig = array(
    // ConstDateTimeFactory::TAB_CONFIG_KEY_ENTITY_TIMEZONE_NAME => 'UTC',
    ConstDateTimeFactory::TAB_CONFIG_KEY_GET_TIMEZONE_NAME => 'Europe/Paris',
    ConstDateTimeFactory::TAB_CONFIG_KEY_GET_DATETIME_FORMAT => 'Y-m-d H:i:s.u',
    ConstDateTimeFactory::TAB_CONFIG_KEY_SET_TIMEZONE_NAME => 'America/New_York',
    ConstDateTimeFactory::TAB_CONFIG_KEY_SET_DATETIME_FORMAT => 'Y-m-d H:i:s.u',
    // ConstDateTimeFactory::TAB_CONFIG_KEY_SAVE_TIMEZONE_NAME => 'UTC',
    ConstDateTimeFactory::TAB_CONFIG_KEY_SAVE_DATETIME_FORMAT => 'Y-m-d H:i:s'
);
$objDateTimeFactory = new DefaultDateTimeFactory($tabConfig);

$objPref = new Preference(array(
    'source' => DateTimeFactoryInterface::class,
    'set' =>  ['type' => 'instance', 'value' => $objDateTimeFactory],
    'option' => [
        'shared' => true
    ]
));
$objProvider->getObjDependencyCollection()->setDependency($objPref);



// Init migration builder
$objMigrationFactory = new DefaultMigrationFactory();
$objMigrationDirBuilder = new DirBuilder(
    $objMigrationFactory
);



// Init migration entity builder
$objMigEntityFactory = new SqlMigEntityFactory($objProvider);
$objMigEntityBuilder = new DefaultBuilder($objMigEntityFactory);


