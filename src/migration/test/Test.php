<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/migration/test/MigrationRepositoryTest.php');

// Use
use liberty_code\migration\migration\model\DefaultMigrationCollection;
use liberty_code\migration_model\migration\library\ConstMigration;
use liberty_code\migration_model\migration\model\MigEntityCollection;



// Init var
$objMigrationCollection = new DefaultMigrationCollection();
$tabDataSrc = array(
    $strRootAppPath . '/vendor/liberty_code/migration/src/migration/test/migration'
);

$objMigrationDirBuilder->setTabDataSrc($tabDataSrc);
$objMigrationDirBuilder->hydrateMigrationCollection($objMigrationCollection);

$objMigEntityCollection = new MigEntityCollection();
$objRemoveMigEntityCollection = new MigEntityCollection();



// Init structure
echo('Test set structure: <pre>');
var_dump($objMigEntityRepo->setStructure());
echo('</pre>');

echo('<br /><br /><br />');



// Test hydrate migration collection
echo('Test hydrate migration collection: <br />');

echo('Before hydrate: <pre>');
foreach($objMigrationCollection->getTabKey() as $strKey)
{
    $objMigration = $objMigrationCollection->getObjMigration($strKey);
    echo('Migration "' . $strKey . '":');echo('<br />');
    echo('Get: config: <pre>');print_r($objMigration->getTabConfig());echo('</pre>');
    echo('Check: is selected: <pre>');var_dump($objMigration->checkIsExecuted());echo('</pre>');
}
echo('</pre>');

$objMigEntityCollectionRepo->loadAll($objMigEntityCollection);
$objMigEntityBuilder->hydrateMigrationCollection(
    $objMigrationCollection,
    $objMigEntityCollection
);

echo('After hydrate: <pre>');
foreach($objMigrationCollection->getTabKey() as $strKey)
{
    $objMigration = $objMigrationCollection->getObjMigration($strKey);
    echo('Migration "' . $strKey . '":');echo('<br />');
    echo('Get: config: <pre>');print_r($objMigration->getTabConfig());echo('</pre>');
    echo('Check: is selected: <pre>');var_dump($objMigration->checkIsExecuted());echo('</pre>');
}
echo('</pre>');

echo('<br /><br /><br />');



// Test hydrate migration entity collection
echo('Test hydrate migration entity collection: <br />');

foreach($objMigrationCollection->getTabKey() as $strKey)
{
    $objMigration = $objMigrationCollection->getObjMigration($strKey);
    $objMigration->setIsExecuted(true);
}

echo('Before hydrate: <pre>');
foreach($objMigEntityCollection->getTabKey() as $strKey)
{
    var_dump($objMigEntityCollection->getItem($strKey)->getTabData());
}
echo('</pre>');

$objMigEntityBuilder->hydrateMigEntityCollection(
    $objMigEntityCollection,
    $objMigrationCollection
);

echo('After hydrate: <pre>');
foreach($objMigEntityCollection->getTabKey() as $strKey)
{
    var_dump($objMigEntityCollection->getItem($strKey)->getTabData());
}
echo('</pre>');

echo('<br /><br /><br />');



// Test migration entity attributes
echo('Test migration entity attributes: <br />');

foreach($objMigEntityCollection->getTabKey() as $strKey)
{
    $objMigEntity = $objMigEntityCollection->getItem($strKey);
    $objMigEntity->setAttributeValue(ConstMigration::ATTRIBUTE_KEY_IS_EXECUTED, true);

    echo('Test validation migration entity:');
    try
    {
        $objMigEntity->setAttributeValue(ConstMigration::ATTRIBUTE_KEY_IS_EXECUTED, 'test');
    }
    catch(Exception $e)
    {
        echo(get_class($e) . ' - ' . $e->getMessage().'<br /><br />');
    }
}

echo('<br /><br /><br />');



// Test re-hydrate migration entity collection
echo('Test re-hydrate migration entity collection: <br />');

$objMigrationCollection->removeMigration('TestMigration1Update');
$objMigrationCollection->removeMigration('TestMigration3');
$objMigrationCollection->removeMigration('TestMigration5');
$objMigrationCollection->removeMigration('TestMigration9');

$objMigrationCollection->getObjMigration('TestMigration7')->setIsExecuted(false);
$objMigrationCollection->getObjMigration('TestMigration8')->setIsExecuted(false);

$objMigEntityBuilder->hydrateMigEntityCollection(
    $objMigEntityCollection,
    $objMigrationCollection,
    $objRemoveMigEntityCollection
);

echo('After hydrate: migration entity collection <pre>');
foreach($objMigEntityCollection->getTabKey() as $strKey)
{
    var_dump($objMigEntityCollection->getItem($strKey)->getTabData());
}
echo('</pre>');

echo('After hydrate: removed migration entity collection <pre>');
foreach($objRemoveMigEntityCollection->getTabKey() as $strKey)
{
    var_dump($objRemoveMigEntityCollection->getItem($strKey)->getTabData());
}
echo('</pre>');

echo('<br /><br /><br />');



// Test get migration entity
echo('Test get migration entity: <br />');
foreach($objMigrationCollection->getTabKey() as $strKey)
{
    $objMigEntity = $objMigEntityCollection->getObjMigEntityFromKey($strKey);
    echo('Get migration entity "' . $strKey . '": <pre>');
    print_r(
        (!is_null($objMigEntity)) ?
            $objMigEntity->getTabData() :
            null
    );
    echo('</pre>');
}

echo('<br /><br /><br />');



// Test save migration entity
echo('Test save migration entity: ');
$tabError = array();
if($objMigEntityCollection->checkValid(null, null, $tabError))
{
    echo('<pre>');var_dump($objMigEntityCollectionRepo->save($objMigEntityCollection));echo('</pre>');
}
else
{
    echo('Validation failed:<pre>');var_dump($tabError);echo('</pre>');
}

echo('<br /><br /><br />');



// Test remove migration entity
echo('Test remove migration entity: ');
$tabError = array();
if($objRemoveMigEntityCollection->checkValid(null, null, $tabError))
{
    echo('<pre>');var_dump($objMigEntityCollectionRepo->remove($objRemoveMigEntityCollection));echo('</pre>');
}
else
{
    echo('Validation failed:<pre>');var_dump($tabError);echo('</pre>');
}

echo('<br /><br /><br />');



// Remove test database, if required
require_once($strRootAppPath . '/src/migration/test/db/HelpRemoveDbTest.php');


