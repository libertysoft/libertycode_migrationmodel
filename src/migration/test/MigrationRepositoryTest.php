<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/migration/test/db/HelpCreateDbTest.php');
require_once($strRootAppPath . '/src/migration/test/MigrationTest.php');

// Use
use liberty_code\di\dependency\preference\model\Preference;

use liberty_code\migration_model\migration\sql\model\repository\SqlMigEntityRepository;
use liberty_code\migration_model\migration\sql\model\repository\SqlMigEntityCollectionRepository;



// Init repository
$objMigEntityRepo = new SqlMigEntityRepository($objTablePersistor);

$objPref = new Preference(array(
    'source' => SqlMigEntityRepository::class,
    'set' =>  ['type' => 'instance', 'value' => $objMigEntityRepo],
    'option' => [
        'shared' => true
    ]
));
$objProvider->getObjDependencyCollection()->setDependency($objPref);

// Init collection repository
$objMigEntityCollectionRepo = new SqlMigEntityCollectionRepository(
    $objMigEntityFactory,
    $objMigEntityRepo
);


