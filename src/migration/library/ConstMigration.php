<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration_model\migration\library;



class ConstMigration
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_ID = 'intAttrId';
    const ATTRIBUTE_KEY_DT_CREATE = 'attrDtCreate';
    const ATTRIBUTE_KEY_DT_UPDATE = 'attrDtUpdate';
    const ATTRIBUTE_KEY_KEY = 'strAttrKey';
    const ATTRIBUTE_KEY_IS_EXECUTED = 'boolAttrIsExecuted';

    const ATTRIBUTE_NAME_SAVE_MIG_ID = 'mig_id';
    const ATTRIBUTE_NAME_SAVE_MIG_DT_CREATE = 'mig_dt_create';
    const ATTRIBUTE_NAME_SAVE_MIG_DT_UPDATE = 'mig_dt_update';
    const ATTRIBUTE_NAME_SAVE_MIG_KEY = 'mig_key';
    const ATTRIBUTE_NAME_SAVE_MIG_IS_EXECUTED = 'mig_is_executed';



}