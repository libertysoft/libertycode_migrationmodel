<?php
/**
 * This class allows to define SQL migration entity class.
 * SQL migration entity allows to design a migration entity,
 * using SQL rules for attributes validation.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration_model\migration\sql\model;

use liberty_code\migration_model\migration\model\MigEntity;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\migration_model\migration\library\ConstMigration;
use liberty_code\migration_model\migration\sql\model\repository\SqlMigEntityRepository;



class SqlMigEntity extends MigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * SQL migration entity repository instance.
     * @var null|SqlMigEntityRepository
     */
    protected $objRepository;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param SqlMigEntityRepository $objRepository = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null,
        SqlMigEntityRepository $objRepository = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $tabValue,
            $objValidator,
            $objDateTimeFactory
        );

        // Init SQL migration entity repository
        $this->setRepository($objRepository);
    }





    // Methods events
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanOnBeforeSetValue($key, $value)
    {
        // Process per attribute key
        switch($key)
        {
            // Case attribute id
            case ConstMigration::ATTRIBUTE_KEY_ID:
                // Reset check is new to true, if required
                if(
                    is_null($value) &&
                    (!$this->checkIsNew())
                )
                {
                    parent::setIsNew(true);
                }
                // Reset check is new to false, if required
                else if(
                    is_int($value) &&
                    ($value > 0) &&
                    $this->checkIsNew()
                )
                {
                    parent::setIsNew(false);
                }

                break;
        }
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init var
        $result = parent::getTabRuleConfig();
        $objRepository = $this->getObjRepository();

        // Get rule configurations, if required
        if(!is_null($objRepository))
        {
            $result = array_merge(
                $result,
                array(
                    ConstMigration::ATTRIBUTE_KEY_ID => [
                        [
                            'group_sub_rule_or',
                            [
                                'rule_config' => [
                                    'is-null' => [
                                        'is_null',
                                        [
                                            'callable',
                                            [
                                                'valid_callable' => function() {return $this->checkIsNew();}
                                            ]
                                        ]
                                    ],
                                    'is-valid-id' => [
                                        [
                                            'type_numeric',
                                            ['integer_only_require' => true]
                                        ],
                                        [
                                            'compare_greater',
                                            [
                                                'compare_value' => 0,
                                                'equal_enable_require' => false
                                            ]
                                        ],
                                        [
                                            'callable',
                                            [
                                                'valid_callable' => function() {return (!$this->checkIsNew());}
                                            ]
                                        ],
                                        [
                                            'sql_exist',
                                            [
                                                'command_factory' => $objRepository->getObjPersistor()->getObjCommandFactory(),
                                                'table_name' => $objRepository->getStrSqlTableName(),
                                                'column_name' => $this->getAttributeNameSave(ConstMigration::ATTRIBUTE_KEY_ID)
                                            ]
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be null if new, or a valid ID else.'
                            ]
                        ]
                    ],
                    ConstMigration::ATTRIBUTE_KEY_KEY => [
                        'type_string',
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => ['is_empty'],
                                'error_message_pattern' => '%1$s is empty.'
                            ]
                        ],
                        [
                            'sub_rule_not',
                            [
                                'rule_config' => [
                                    [
                                        'sql_exist',
                                        [
                                            'command_factory' => $objRepository->getObjPersistor()->getObjCommandFactory(),
                                            'table_name' => $objRepository->getStrSqlTableName(),
                                            'column_name' => $this->getAttributeNameSave(ConstMigration::ATTRIBUTE_KEY_KEY),
                                            'exclude' => [
                                                $this->getAttributeNameSave(ConstMigration::ATTRIBUTE_KEY_ID) =>
                                                    $this->getAttributeValueSave(ConstMigration::ATTRIBUTE_KEY_ID)
                                            ]
                                        ]
                                    ]
                                ],
                                'error_message_pattern' => '%1$s must be unique.'
                            ]
                        ]
                    ]
                )
            );
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function setIsNew($boolIsNew)
    {
        // Init var
        $intAttrId = $this->getAttributeValue(ConstMigration::ATTRIBUTE_KEY_ID);

        // Call parent method
        parent::setIsNew($boolIsNew);

        // Reset attribute id, if required
        if($boolIsNew && (!is_null($intAttrId)))
        {
            $this->setAttributeValue(ConstMigration::ATTRIBUTE_KEY_ID, null);
        }
        // Else: Check valid attribute id
        else if(!$boolIsNew)
        {
            $this->setAttributeValid(ConstMigration::ATTRIBUTE_KEY_ID);
        }
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get SQL migration entity repository object.
     *
     * @return null|SqlMigEntityRepository
     */
    public function getObjRepository()
    {
        // Return result
        return $this->objRepository;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set SQL migration entity repository object.
     *
     * @param SqlMigEntityRepository $objRepository = null
     */
    public function setRepository(SqlMigEntityRepository $objRepository = null)
    {
        $this->objRepository = $objRepository;
    }



}