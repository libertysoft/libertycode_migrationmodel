<?php
/**
 * This class allows to define SQL migration entity repository class.
 * SQL migration entity repository uses SQL table persistence.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration_model\migration\sql\model\repository;

use liberty_code\migration_model\migration\model\repository\MigEntityRepository;

use liberty_code\sql\persistence\library\ConstPersistor;
use liberty_code\sql\persistence\table\model\TablePersistor;
use liberty_code\migration_model\migration\library\ConstMigration;
use liberty_code\migration_model\migration\sql\library\ConstSqlMigration;



/**
 * @method null|TablePersistor getObjPersistor() @inheritdoc
 */
class SqlMigEntityRepository extends MigEntityRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param TablePersistor $objPersistor = null
     */
    public function __construct(TablePersistor $objPersistor = null)
    {
        // Call parent constructor
        parent::__construct($objPersistor);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixPersistorClassPath()
    {
        // Return result
        return TablePersistor::class;
    }



    /**
     * Get SQL table name where migrations stored.
     * Overwrite it to implement specific table name.
     *
     * @return string
     */
    public function getStrSqlTableName()
    {
        // Return result
        return ConstSqlMigration::SQL_TABLE_NAME_MIG;
    }



    /**
     * @inheritdoc
     */
    protected function getTabFixPersistorConfig()
    {
        // Return result
        return array(
            ConstPersistor::TAB_CONFIG_KEY_TABLE_NAME => $this->getStrSqlTableName(),
            ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID  => ConstMigration::ATTRIBUTE_NAME_SAVE_MIG_ID
        );
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set migration structure, if required.
     * Return true if correctly run, false else.
     *
     * @return boolean
     */
    public function setStructure()
    {
        // Init var
        $objConnection = $this
            ->getObjPersistor()
            ->getObjCommandFactory()
            ->getObjConnection();

        // Create migration table, if required
        $strSql = sprintf(
            'CREATE TABLE IF NOT EXISTS %1$s (
                %2$s int(10) unsigned NOT NULL AUTO_INCREMENT,
                %3$s datetime NOT NULL,
                %4$s datetime NOT NULL,
                %5$s varchar(100) NOT NULL,
                %6$s tinyint(1) NOT NULL,
                PRIMARY KEY (%2$s),
                UNIQUE KEY %5$s (%5$s)
            );',
            $objConnection->getStrEscapeName($this->getStrSqlTableName()),
            $objConnection->getStrEscapeName(ConstMigration::ATTRIBUTE_NAME_SAVE_MIG_ID),
            $objConnection->getStrEscapeName(ConstMigration::ATTRIBUTE_NAME_SAVE_MIG_DT_CREATE),
            $objConnection->getStrEscapeName(ConstMigration::ATTRIBUTE_NAME_SAVE_MIG_DT_UPDATE),
            $objConnection->getStrEscapeName(ConstMigration::ATTRIBUTE_NAME_SAVE_MIG_KEY),
            $objConnection->getStrEscapeName(ConstMigration::ATTRIBUTE_NAME_SAVE_MIG_IS_EXECUTED)
        );
        $result = ($objConnection->execute($strSql) !== false);

        // Return result
        return $result;
    }



}


