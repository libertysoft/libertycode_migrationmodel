<?php
/**
 * This class allows to define SQL migration entity collection repository class.
 * SQL migration entity collection repository uses SQL table persistence.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration_model\migration\sql\model\repository;

use liberty_code\migration_model\migration\model\repository\MigEntityCollectionRepository;

use liberty_code\sql\persistence\table\model\TablePersistor;
use liberty_code\migration_model\migration\model\MigEntityCollection;
use liberty_code\migration_model\migration\model\MigEntityFactory;
use liberty_code\migration_model\migration\sql\model\repository\SqlMigEntityRepository;



/**
 * @method null|SqlMigEntityRepository getObjRepository() @inheritdoc
 * @method null|TablePersistor getObjPersistor() @inheritdoc
 */
class SqlMigEntityCollectionRepository extends MigEntityCollectionRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param SqlMigEntityRepository $objRepository = null
     */
    public function __construct(
        MigEntityFactory $objEntityFactory = null,
        SqlMigEntityRepository $objRepository = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objEntityFactory,
            $objRepository
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixRepositoryClassPath()
    {
        // Return result
        return SqlMigEntityRepository::class;
    }





    // Methods repository
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function loadAll(MigEntityCollection $objMigEntityCollection)
    {
        // Init var
        $strTableNm = $this->getObjRepository()->getStrSqlTableName();
        $query = array(
            'select' => [
                ['pattern' => '*']
            ],
            'from' => [
                $strTableNm
            ]
        );

        // Return result
        return $this->search($objMigEntityCollection, $query);
    }



}