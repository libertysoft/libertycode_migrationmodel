<?php
/**
 * This class allows to define SQL migration entity factory class.
 * SQL migration entity factory is migration entity factory,
 * allows to provide new SQL migration entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration_model\migration\sql\model;

use liberty_code\migration_model\migration\model\MigEntityFactory;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\migration_model\migration\sql\model\SqlMigEntity;
use liberty_code\migration_model\migration\sql\model\repository\SqlMigEntityRepository;



/**
 * @method SqlMigEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 */
class SqlMigEntityFactory extends MigEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => SqlMigEntity::class
        );
    }



    /**
     * @inheritdoc
     * @return SqlMigEntity
     */
    protected function getObjEntityNew(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $objRepository = $this->getObjInstance(SqlMigEntityRepository::class);
        $result = new SqlMigEntity(
            array(),
            $objValidator,
            $objDateTimeFactory,
            $objRepository
        );

        // Return result
        return $result;
    }



}