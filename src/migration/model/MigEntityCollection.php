<?php
/**
 * This class allows to define migration entity collection class.
 * key: migration key name => MigEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration_model\migration\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\migration_model\migration\library\ConstMigration;
use liberty_code\migration_model\migration\model\MigEntity;



/**
 * @method null|MigEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(MigEntity $objMigEntity) @inheritdoc
 */
class MigEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return MigEntity::class;
    }



    /**
     * Get migration entity object,
     * from specified migration key.
     *
     * @param string $strKey
     * @return null|MigEntity
     */
    public function getObjMigEntityFromKey($strKey)
    {
        // Init var
        $result = null;

        // Select space entity, from name
        $tabConfig = array(
            [
                ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_ATTRIBUTE_KEY => ConstMigration::ATTRIBUTE_KEY_KEY,
                ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_OPERATION => ConstEntity::COLLECTION_GET_CONFIG_OPERATION_EQUAL,
                ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_VALUE => $strKey
            ]
        );
        /** @var MigEntity[] $tabSpaceEntity */
        $tabSpaceEntity = array_values($this->getTabItem($tabConfig));

        // Register space entity, if found
        if(isset($tabSpaceEntity[0]))
        {
            $result = $tabSpaceEntity[0];
        }

        // Return result
        return $result;
    }



}