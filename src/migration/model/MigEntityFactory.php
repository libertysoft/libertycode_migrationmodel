<?php
/**
 * This class allows to define migration entity factory class.
 * Migration entity factory allows to provide new migration entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration_model\migration\model;

use liberty_code\model\entity\factory\fix\model\FixEntityFactory;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\migration_model\migration\model\MigEntity;



/**
 * @method MigEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 */
class MigEntityFactory extends FixEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => MigEntity::class
        );
    }



    /**
     * @inheritdoc
     * @return MigEntity
     */
    protected function getObjEntityNew(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $result = new MigEntity(
            array(),
            $objValidator,
            $objDateTimeFactory
        );

        // Return result
        return $result;
    }



}