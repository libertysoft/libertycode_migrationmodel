<?php
/**
 * This class allows to define migration entity repository class.
 * Migration entity repository allows to prepare data from migration entity,
 * to save in persistence.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration_model\migration\model\repository;

use liberty_code\model\repository\multi\fix\model\FixMultiRepository;

use liberty_code\model\entity\repository\api\SaveEntityInterface;
use liberty_code\model\repository\library\ConstRepository;
use liberty_code\model\repository\multi\library\ConstMultiRepository;
use liberty_code\model\datetime\library\ToolBoxDateTime;
use liberty_code\migration_model\migration\library\ConstMigration;
use liberty_code\migration_model\migration\model\MigEntity;



class MigEntityRepository extends FixMultiRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get fixed configuration array for persistence.
     * Overwrite it to implement specific configuration array.
     *
     * @return array
     */
    protected function getTabFixPersistorConfig()
    {
        // Return result
        return array();
    }



    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Init var
        $result = array_merge(
            array(
                ConstRepository::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => MigEntity::class,
                ConstMultiRepository::TAB_CONFIG_KEY_ATTRIBUTE_KEY_ID => ConstMigration::ATTRIBUTE_KEY_ID
            ),
            $this->getTabFixPersistorConfig()
        );

        // Return result
        return $result;
    }





    // Methods repository
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function save(
        SaveEntityInterface $objEntity,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Set check arguments
        $this->setCheckValidEntity($objEntity);

        // Update datetime create, update
        ToolBoxDateTime::hydrateEntityAttrDtCreateUpdate(
            $objEntity,
            ConstMigration::ATTRIBUTE_KEY_DT_CREATE,
            ConstMigration::ATTRIBUTE_KEY_DT_UPDATE
        );

        // Return result: call parent method
        return parent::save($objEntity, $tabConfig, $tabInfo);
    }



}


