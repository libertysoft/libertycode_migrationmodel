<?php
/**
 * This class allows to define migration entity collection repository class.
 * Migration entity collection repository allows to prepare data from migration entity collection,
 * to save in persistence.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration_model\migration\model\repository;

use liberty_code\model\repository\multi\fix\model\FixMultiCollectionRepository;

use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\datetime\library\ToolBoxDateTime;
use liberty_code\migration_model\migration\library\ConstMigration;
use liberty_code\migration_model\migration\model\MigEntityFactory;
use liberty_code\migration_model\migration\model\MigEntityCollection;
use liberty_code\migration_model\migration\model\repository\MigEntityRepository;



/**
 * @method null|MigEntityFactory getObjEntityFactory() @inheritdoc
 * @method null|MigEntityRepository getObjRepository() @inheritdoc
 */
abstract class MigEntityCollectionRepository extends FixMultiCollectionRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param MigEntityFactory $objEntityFactory = null
     * @param MigEntityRepository $objRepository = null
     */
    public function __construct(
        MigEntityFactory $objEntityFactory = null,
        MigEntityRepository $objRepository = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objEntityFactory,
            $objRepository
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixEntityFactoryClassPath()
    {
        // Return result
        return MigEntityFactory::class;
    }



    /**
     * @inheritdoc
     */
    protected function getStrFixRepositoryClassPath()
    {
        // Return result
        return MigEntityRepository::class;
    }





    // Methods repository
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function save(
        EntityCollectionInterface $objCollection,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Set check arguments
        $this->setCheckValidCollection($objCollection);

        // Update datetime create, update, on entities
        ToolBoxDateTime::hydrateEntityCollectionAttrDtCreateUpdate(
            $objCollection,
            ConstMigration::ATTRIBUTE_KEY_DT_CREATE,
            ConstMigration::ATTRIBUTE_KEY_DT_UPDATE
        );

        // Return result: call parent method
        return parent::save($objCollection, $tabConfig, $tabInfo);
    }



    /**
     * Load all migration entities,
     * on specified migration entity collection object.
     * Return true if all entities success, false if an error occurs on at least one entity.
     *
     * @param MigEntityCollection $objMigEntityCollection
     * @return boolean
     */
    abstract public function loadAll(MigEntityCollection $objMigEntityCollection);



}