<?php
/**
 * Description :
 * This class allows to define default builder class.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration_model\build\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\migration_model\build\api\BuilderInterface;

use liberty_code\migration\migration\api\MigrationCollectionInterface;
use liberty_code\migration_model\migration\library\ConstMigration;
use liberty_code\migration_model\migration\model\MigEntityCollection;
use liberty_code\migration_model\migration\model\MigEntityFactory;
use liberty_code\migration_model\build\library\ConstBuilder;
use liberty_code\migration_model\build\exception\MigEntityFactoryInvalidFormatException;



/**
 * @method null|MigEntityFactory getObjMigEntityFactory() Get migration entity factory object.
 * @method void setObjMigEntityFactory(null|MigEntityFactory $objMigEntityFactory) Set migration entity factory object.
 */
class DefaultBuilder extends FixBean implements BuilderInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param MigEntityFactory $objMigEntityFactory
     */
    public function __construct(MigEntityFactory $objMigEntityFactory)
    {
        // Call parent constructor
        parent::__construct();

        // Init migration factory
        $this->setObjMigEntityFactory($objMigEntityFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstBuilder::DATA_KEY_DEFAULT_MIG_ENTITY_FACTORY))
        {
            $this->beanAdd(ConstBuilder::DATA_KEY_DEFAULT_MIG_ENTITY_FACTORY, null);
        }
    }



    /**
     * @inheritdoc
     */
    public function hydrateMigrationCollection(
        MigrationCollectionInterface $objMigrationCollection,
        MigEntityCollection $objMigEntityCollection
    )
    {
        // Run each migration entity
        $tabKey = $objMigEntityCollection->getTabKey();
        foreach($tabKey as $strKey)
        {
            $objMigEntity = $objMigEntityCollection->getItem($strKey);

            // Hydrate migration, from migration entity, if required (migration found)
            $strMigEntityKey = $objMigEntity->getAttributeValue(ConstMigration::ATTRIBUTE_KEY_KEY);
            $objMigration = $objMigrationCollection->getObjMigration($strMigEntityKey);
            if(!is_null($objMigration))
            {
                // Check is executed
                $boolMigEntityIsExecuted = $objMigEntity->getAttributeValue(ConstMigration::ATTRIBUTE_KEY_IS_EXECUTED);
                $objMigration->setIsExecuted($boolMigEntityIsExecuted);
            }
        }
    }



    /**
     * @inheritdoc
     */
    public function hydrateMigEntityCollection(
        MigEntityCollection $objMigEntityCollection,
        MigrationCollectionInterface $objMigrationCollection,
        MigEntityCollection $objRemoveMigEntityCollection = null
    )
    {
        // Run each migration
        $tabKey = $objMigrationCollection->getTabKey();
        foreach($tabKey as $strKey)
        {
            $objMigration = $objMigrationCollection->getObjMigration($strKey);

            // Get migration entity, from migration, if found
            $objMigEntity = null;
            $tabEntityKey = $objMigEntityCollection->getTabKey();
            for($intCpt = 0; ($intCpt < count($tabEntityKey)) && is_null($objMigEntity); $intCpt++)
            {
                $objMigEntity = $objMigEntityCollection->getItem($tabEntityKey[$intCpt]);
                $strMigEntityKey = $objMigEntity->getAttributeValue(ConstMigration::ATTRIBUTE_KEY_KEY);
                $objMigEntity = (
                    ($strMigEntityKey == $strKey) ?
                        $objMigEntity :
                        null
                );
            }

            // Get migration entity
            $objMigEntity = (
                is_null($objMigEntity) ?
                    $this->getObjMigEntityFactory()->getObjEntity() :
                    $objMigEntity
            );

            // Hydrate migration entity, from migration, if required (migration entity found)
            if(!is_null($objMigEntity))
            {
                // Set key
                $objMigEntity->setAttributeValue(ConstMigration::ATTRIBUTE_KEY_KEY, $objMigration->getStrKey());

                // Set is executed
                $objMigEntity->setAttributeValue(ConstMigration::ATTRIBUTE_KEY_IS_EXECUTED, $objMigration->checkIsExecuted());
            }

            // Register migration entity
            $objMigEntityCollection->setItem($objMigEntity);
        }

        // Run each migration entity
        $tabKey = $objMigEntityCollection->getTabKey();
        foreach($tabKey as $strKey)
        {
            $objMigEntity = $objMigEntityCollection->getItem($strKey);
            $strMigEntityKey = $objMigEntity->getAttributeValue(ConstMigration::ATTRIBUTE_KEY_KEY);

            // Remove migration entity, if required (migration not found)
            if(!$objMigrationCollection->checkExists($strMigEntityKey))
            {
                $objMigEntityCollection->removeItem($objMigEntity->getStrKey());
                if(!is_null($objRemoveMigEntityCollection))
                {
                    $objRemoveMigEntityCollection->setItem($objMigEntity);
                }
            }
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstBuilder::DATA_KEY_DEFAULT_MIG_ENTITY_FACTORY
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstBuilder::DATA_KEY_DEFAULT_MIG_ENTITY_FACTORY:
                    MigEntityFactoryInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }



}