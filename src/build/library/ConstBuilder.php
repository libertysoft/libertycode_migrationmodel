<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration_model\build\library;



class ConstBuilder
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************
	
	// Properties
    const DATA_KEY_DEFAULT_MIG_ENTITY_FACTORY = 'objMigEntityFactory';
	
	
	
	// Exception message
    const EXCEPT_MSG_MIG_ENTITY_FACTORY_INVALID_FORMAT =
        'Following migration entity factory "%1$s" invalid! It must be null or a factory object.';



}