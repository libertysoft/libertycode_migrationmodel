<?php
/**
 * Description :
 * This class allows to describe behavior of builder class.
 * Builder allows to hydrate existing migration and migration entity collection instances.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration_model\build\api;

use liberty_code\migration\migration\api\MigrationCollectionInterface;
use liberty_code\migration_model\migration\model\MigEntityCollection;



interface BuilderInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified existing migration collection,
     * from specified migration entity collection.
     *
     * @param MigrationCollectionInterface $objMigrationCollection
     * @param MigEntityCollection $objMigEntityCollection
     */
    public function hydrateMigrationCollection(
        MigrationCollectionInterface $objMigrationCollection,
        MigEntityCollection $objMigEntityCollection
    );



    /**
     * Hydrate specified existing migration entity collection,
     * from specified migration collection.
     *
     * @param MigrationCollectionInterface $objMigrationCollection
     * @param MigEntityCollection $objMigEntityCollection
     * @param MigEntityCollection $objRemoveMigEntityCollection = null
     */
    public function hydrateMigEntityCollection(
        MigEntityCollection $objMigEntityCollection,
        MigrationCollectionInterface $objMigrationCollection,
        MigEntityCollection $objRemoveMigEntityCollection = null
    );
}