<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\migration_model\build\exception;

use Exception;
use liberty_code\migration_model\migration\model\MigEntityFactory;
use liberty_code\migration_model\build\library\ConstBuilder;



class MigEntityFactoryInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $factory
     */
	public function __construct($factory)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstBuilder::EXCEPT_MSG_MIG_ENTITY_FACTORY_INVALID_FORMAT,
            mb_strimwidth(strval($factory), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified factory has valid format.
	 * 
     * @param mixed $factory
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($factory)
    {
		// Init var
		$result = (
			(is_null($factory)) ||
			($factory instanceof MigEntityFactory)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($factory);
		}
		
		// Return result
		return $result;
    }
	
	
	
}