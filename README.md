LibertyCode_MigrationModel
==========================



Description
-----------

Library contains migration model components, 
to manage application migrations.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Framework library implementation requirement
-----------------------------------------

1. Library repository: liberty_code/validation: version 1.0

    - Standard rules implementation (or equivalent): 
        
        Validator provided on entities, 
        must contain all standard rules, 
        added on its rule collection.
        
    - Validator rules implementation (or equivalent): 
    
        Validator provided on entities, 
        must contain all validator rules, 
        added on its rule collection.
        Each validator rule must use validator,
        with same implementation as validator provided on entities.
        
2. Library repository: liberty_code/sql: version 1.0

    - Validation SQL rules implementation (or equivalent): 
        
        Validator provided on entities, 
        must contain all SQL rules, 
        added on its rule collection.
        
---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/migration_model ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/migration_model": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

#### Migration

Migration entity system allows to design data structure for migration,
to manage, retrieve and save its data in persistence.

_Elements_

- MigEntity

    Extends save configured entity features. 
    Allows to design a migration entity, 
    to save it.

- MigEntityCollection

    Extends fixed entity collection features. 
    Allows to design collection of migration entities.

- MigEntityFactory

    Extends fixed entity factory features. 
    Allows to design migration entity factory, 
    to provide migration entity objects.

- MigEntityRepository

    Extends fixed multi repository features.
    Allows to design migration entity repository,
    to save data, from migration entity, in persistence.

- MigEntityCollectionRepository

    Extends fixed multi collection repository features. 
    Allows to design migration entity collection repository,
    to save data, from migration entity collection, in persistence.
    
- SqlMigEntity

    Extends migration entity features. 
    Uses SQL rules for attributes validation.
    
- SqlMigEntityFactory

    Extends migration entity factory features. 
    Provides SQL migration entity objects.
    
- SqlMigEntityRepository

    Extends migration entity repository features.
    Uses SQL table persistor, as persistence. 
    
- SqlMigEntityCollectionRepository

    Extends migration entity collection repository features. 
    Uses SQL table persistor, as persistence. 
    
```php
// Get migration entity factory
use liberty_code\migration\entity\model\MigEntityFactory;
$migEntityFactory = new MigEntityFactory($provider);
...
// Get new migration entity
$migEntity = $migEntityFactory->getObjEntity();
...
```

#### Builder

Builder allows to hydrate existing migration with migration entity collections 
and vice versa.

```php
// Get migration entity collection
use liberty_code\migration\entity\model\MigEntityCollection;
$migEntityCollection = new MigEntityCollection();
...
// Get migration entity builder
use liberty_code\migration\entity\build\model\DefaultBuilder;
$migEntityBuilder = new DefaultBuilder($migEntityFactory);
...
// Hydrate migration entity collection
$migEntityBuilder->hydrateMigEntityCollection(
    $migEntityCollection, 
    $migrationCollection
);
...
foreach($migEntityCollection->getTabKey() as $key) {
    echo($migEntityCollection->getItem($key)->strAttrKey .'<br />');
}
/**
 * Show: 
 * Migration key 1
 * ...
 * Migration key N
 */
...
```

---


